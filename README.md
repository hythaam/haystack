# haystack

a simple webapp for demoing chaff-and-winnow pseudo-encryption.
I'll be adding to this project slowly, as it is mostly an experiment
in using typescript and vuejs. I'd like to eventually write the
backend in rust with a few more features. I'll get to that documentation
eventually, but for now it'll just be a dumping ground.
