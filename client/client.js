const Vue = require('Vue');
const app = require('./app.js');

const vue = new Vue({
    el: '#vueapp',
    render: function(createElement) {
        return createElement(app);
    }
});
