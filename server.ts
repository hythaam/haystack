import { createServer, Server } from 'http';
import * as path from "path";
import * as Express from "express";
import * as SocketIO from 'socket.io';

class HaystackServer {
    private static readonly PORT: number = 3000;
    private app: Express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;
    
    constructor() {
        this.app = Express();
        this.port = process.env.PORT || HaystackServer.PORT;
        this.server = createServer(this.app);
        this.io = SocketIO(this.server);
    }

    public listen(): Express.Application {

        this.app.use(
            Express.static(path.join(__dirname, "../client"))
        );

        this.server.listen(this.port, () => {
            console.log('Haystack server running on port %s', this.port);
        });

        this.io.on('connect', (socket: any) => {
            console.log('Client %s connected', socket.id);
            socket.on('message', (m) => {
                console.log('[server](message): %s', JSON.stringify(m));
                this.io.emit('message', m);
            });
            socket.on('disconnect', () => {
                console.log('Client %s disconnected', socket.id);
            });
        });

       
    }
}

const app = new HaystackServer().listen();
export { app };
