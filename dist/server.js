"use strict";
exports.__esModule = true;
var http_1 = require("http");
var path = require("path");
var Express = require("express");
var SocketIO = require("socket.io");
var HaystackServer = /** @class */ (function () {
    function HaystackServer() {
        this.app = Express();
        this.port = process.env.PORT || HaystackServer.PORT;
        this.server = http_1.createServer(this.app);
        this.io = SocketIO(this.server);
    }
    HaystackServer.prototype.listen = function () {
        var _this = this;
        this.app.use(Express.static(path.join(__dirname, "../client")));
        this.server.listen(this.port, function () {
            console.log('Haystack server running on port %s', _this.port);
        });
        this.io.on('connect', function (socket) {
            console.log('Client %s connected', socket.id);
            socket.on('message', function (m) {
                console.log('[server](message): %s', JSON.stringify(m));
                _this.io.emit('message', m);
            });
            socket.on('disconnect', function () {
                console.log('Client %s disconnected', socket.id);
            });
        });
    };
    HaystackServer.PORT = 3000;
    return HaystackServer;
}());
var app = new HaystackServer().listen();
exports.app = app;
